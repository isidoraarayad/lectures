## Changes since the last year (11/02)

Following our own evaluation and the student feedback we are implementing the following changes this year:

* Moved the minitests to Mondays or the midterm week.
* Changed the Q&A sessions on Friday into a forum setting, where the course participants answer each other's question.
* Shared two example exams and the solutions on the course website
* Provide solution outlines on the course website.
* Misc minor tweaks of content.
