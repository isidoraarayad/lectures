# Delft 2020 course run

Welcome to the 2020 run of the solid state physics course (TN2844)!

## These lecture notes

This website is the main source of course information, and we keep it up to date. We only use brightspace for course announcements, and for getting student lists, so do sign up there.

We welcome feedback and contributions to these lecture notes, so if you have any suggestions, please open an [issue](http://gitlab.kwant-project.org/solidstate/lectures/issues/new).

## Course language

The learning materials are in English, and some of the course team members do not speak Dutch, but you can always find somebody who does, and if you find it easier to express yourself in Dutch—go ahead.

## The book

As an additional source of learning materials we use the book by Steve Simon "Oxford solid state basics". It has a much more in-depth and systematic explanation of the content, and therefore we recommend to read it in addition to following the lecture notes. The exam materials, however, are entirely based on what we cover in the lecture notes and the exercises.

## Course forum

Whenever you have a question outside of regular hours, reach the course team and your fellow course participants in the [course forum](https://forum.solidstate.quantumtinkerer.tudelft.nl/). We invite you to register right away.

We invite to have any course-related discussions in the forum, and we aim to help you by answering questions.
As a quick reminder, we expect you to be polite to each other and the course team, and to participate in the discussions constructively.

## Prior knowledge

The required prior knowledge for a lecture is listed in the lecture notes. Refreshing this knowledge before a lecture will help you in the course. 

## Course activities

* Tuesday morning and Thursday afternoon we have lectures followed by the exercise classes, where you work out the problems listed in the lecture notes.  
  Each day corresponds to one page of the lecture notes.
* Fridays we have a Q&A session, where course participants answer each other's questions, chaired by the course team members.
* The minitests (see below) take place 24/02, 12/03, and 30/03. 
* The final exam takes place 14/04, and the retake exam 02/07.

## Schedule

The course schedule is available on the TU Delft [timetable website](https://mytimetable.tudelft.nl/).

## Assessment

* During the course you need to take 3 minitests and a final exam.
* Each minitest will focus on the materials of the preceding two weeks, but may use other material we covered.
* Two of the best minitest results each count for 10% of the final grade.
* You get 0 points for the minitests that you missed, so you definitely lose points if you miss more than one.
* The final exam counts for the remaining 80% of the grade.
* At the retake exam the minitest scores count if they increase the grade.

The course team aims to make the difficulty of the exam problems, the minitest problems, and the exercises as similar as possible.

## Course team

### Lecturers

* Anton Akhmerov, [ssp@antonakhmerov.org](mailto:ssp@antonakhmerov.org), office F334
* Toeno van der Sar, [t.vandersar@tudelft.nl](mailto:t.vandersar@tudelft.nl), office D108

### Teaching Assistants

* Kostas Vilkelis
* Michael Borst
* Radoica Draskic
* Bowy La Rivière
* Lars kleyn Winkel
* Isidora Araya Day
* Sathish Kumar RK
